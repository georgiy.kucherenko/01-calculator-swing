package org.example.gui_swing;

import org.example.calculator.utils.ArithmeticOperation;
import org.example.calculator.models.ResultOfCalculation;
import org.example.calculator.service.CalculatorService;

import javax.swing.*;
import javax.swing.event.UndoableEditListener;
import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.example.calculator.utils.ArithmeticOperation.values;

/**
 * This is class for GUI of CalculatorService. This GUI is made with SWING library.
 *
 * @author Gerogiy Kucherenko
 */
public class MainWindow extends JFrame {
    private static final String ERROR_HAPPENED = "Error. Check history";
    private static final String REGEX_DOUBLE = "^-?\\d+\\.\\d*$|^-?\\d+$";
    private final CalculatorService calculatorService;
    private JLabel resultLabel;
    private JComboBox<String> listOfOperations;
    private JTextField textField1;
    private JTextField textField2;
    private JCheckBox onFlyCheckBox;
    private DefaultListModel<String> historyListModel;

    /**
     * Public constructor.
     *
     * @param calculatorService instance of {@link CalculatorService} class
     */
    public MainWindow(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
        initializeTabs();
        configureFrame();
    }

    private void initializeTabs() {
        JTabbedPane tabbedPane = new JTabbedPane();
        initializeMainTab(tabbedPane);
        initializeLogsTab(tabbedPane);
        getContentPane().add(tabbedPane, BorderLayout.CENTER);
    }

    private void initializeLogsTab(JTabbedPane tabbedPane) {
        JPanel jPanelLogs = new JPanel(new GridLayout());
        tabbedPane.addTab("History", jPanelLogs);

        JList<String> historyList = new JList<>();
        historyListModel = new DefaultListModel<>();
        historyList.setModel(historyListModel);
        JScrollPane historyScrollPane = new JScrollPane(historyList);
        jPanelLogs.add(historyScrollPane);
    }

    private void initializeMainTab(JTabbedPane tabbedPane) {
        JPanel jPanelMain = new JPanel(new GridBagLayout());
        tabbedPane.addTab("Calculator", jPanelMain);

        JButton calculateButton = new JButton("Calculate");
        calculateButton.setToolTipText("Push the button to make the calculation");
        calculateButton.addActionListener(createCalculateListener());

        listOfOperations = new JComboBox<>(getArrayOfOperationsTitles());
        listOfOperations.setToolTipText("Chose a type of arithmetic operation");

        textField1 = new JTextField(10);
        textField1.setToolTipText("type a Numeric value here");
        textField1.getDocument().addUndoableEditListener(createColoringUndoableListener());

        textField2 = new JTextField(10);
        textField2.setToolTipText("type a Numeric value here");
        textField2.getDocument().addUndoableEditListener(createBlockingUndoableListener());

        resultLabel = new JLabel();

        JLabel infoLabel = new JLabel("RESULT:");

        onFlyCheckBox = new JCheckBox("Calculate on the fly");
        onFlyCheckBox.addItemListener(e -> calculateButton.setEnabled(!onFlyCheckBox.isSelected()));

        setupPanelLayout(jPanelMain, infoLabel, calculateButton, onFlyCheckBox);
    }

    private void setupPanelLayout(JPanel jPanelMain, JLabel infoLabel, JButton calculateButton,
                                  JCheckBox onFlyCheckBox) {

        GridBagConstraints constraints = new GridBagConstraints();

        constraints.anchor = GridBagConstraints.NORTH;
        constraints.insets = new Insets(10, 6, 6, 6);

        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weighty = 0;
        constraints.weightx = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        jPanelMain.add(textField1, constraints);

        constraints.fill = GridBagConstraints.NONE;
        constraints.gridx = 1;
        constraints.weightx = 0;
        jPanelMain.add(listOfOperations, constraints);

        constraints.gridx = 2;
        constraints.weightx = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        jPanelMain.add(textField2, constraints);

        constraints.weightx = 1;
        constraints.gridx = 0;
        constraints.gridwidth = 2;
        constraints.gridy = 1;
        constraints.fill = GridBagConstraints.NONE;
        constraints.anchor = GridBagConstraints.SOUTHWEST;
        constraints.weighty = 1;
        jPanelMain.add(onFlyCheckBox, constraints);

        constraints.weightx = 0;
        constraints.gridx = 2;
        constraints.gridwidth = 1;
        constraints.weighty = 0;
        constraints.anchor = GridBagConstraints.SOUTHEAST;
        jPanelMain.add(calculateButton, constraints);

        constraints.weighty = 0;
        constraints.weightx = 0;
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.WEST;
        jPanelMain.add(infoLabel, constraints);

        constraints.gridx = 1;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.EAST;
        jPanelMain.add(resultLabel, constraints);
    }

    private UndoableEditListener createColoringUndoableListener() {
        return new UndoableEListenerImpl
                (
                        (jTextField, event) -> jTextField.setForeground(Color.RED),
                        (jTextField, event) -> {
                            jTextField.setForeground(Color.BLACK);
                            if (onFlyCheckBox.isSelected()) {
                                calculate();
                            }
                        },
                        textField1,
                        REGEX_DOUBLE
                );
    }

    private UndoableEditListener createBlockingUndoableListener() {
        return new UndoableEListenerImpl
                (
                        (jTextField, event) -> event.getEdit().undo(),
                        (jTextField, event) -> {
                            if (onFlyCheckBox.isSelected()) {
                                calculate();
                            }
                        },
                        textField2,
                        REGEX_DOUBLE
                );
    }

    private ActionListener createCalculateListener() {
        return e -> {
            if (checkIfTextFieldsAreEmpty()) {
                resultLabel.setText(ERROR_HAPPENED);
                historyListModel.add(0, "Fill in both text fields before calculating");
                return;
            }
            calculate();
        };
    }

    private boolean checkIfTextFieldsAreEmpty() {
        return textField1.getText().isEmpty() || textField2.getText().isEmpty();
    }

    private void calculate() {
        try {
            ResultOfCalculation resultOfCalculation = calculatorService.getResultOfCalculation(
                    Double.valueOf(textField1.getText()),
                    Double.valueOf(textField2.getText()),
                    getArithmeticOperation());

            resultLabel.setText(calculatorService.getRoundedResult(resultOfCalculation.getResult()));
            historyListModel.add(0, resultOfCalculation.getStringForLog());
        } catch (ArithmeticException | NumberFormatException | UnsupportedOperationException exception) {
            resultLabel.setText(ERROR_HAPPENED);
            historyListModel.add(0, exception.getMessage());
        }
    }

    private ArithmeticOperation getArithmeticOperation() throws UnsupportedOperationException {
        String selectedOperation = (String) listOfOperations.getSelectedItem();
        return Arrays.stream(values())
                .filter(a -> a.getTitle().equals(selectedOperation))
                .findFirst().orElseThrow(() -> new UnsupportedOperationException("This operation is not active"));
    }

    private static String[] getArrayOfOperationsTitles() {
        ArithmeticOperation[] arrayOfOperations = ArithmeticOperation.values();
        List<String> listOfOperationsTitles = Arrays.stream(arrayOfOperations)
                .map(ArithmeticOperation::getTitle)
                .collect(Collectors.toList());
        String[] arrayOfOperationsTitles = new String[listOfOperationsTitles.size()];
        listOfOperationsTitles.toArray(arrayOfOperationsTitles);
        return arrayOfOperationsTitles;
    }

    private void configureFrame() {
        setTitle("Calculator");
        setLayout(new GridLayout());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(400, 400);
        setVisible(true);
    }
}
