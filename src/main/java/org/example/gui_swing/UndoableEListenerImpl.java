package org.example.gui_swing;

import javax.swing.*;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class helps to arrange a mask for {@link JTextField}.
 * Fields of this class:
 * <ul>
 * <li>string value storing the regex-mask
 * <li>link to JTextField
 * <li>BiConsumer that is in charge of situation when the mask doesn't match
 * <li>BiConsumer (optional) that is in charge of situation when the mask matches
 * </ul>
 *
 * @author Georgiy Kucherenko
 */
public class UndoableEListenerImpl implements UndoableEditListener {
    private final BiConsumer<JTextField, UndoableEditEvent> modifier;
    private BiConsumer<JTextField, UndoableEditEvent> optionalModifier;
    private final JTextField textField;
    private final Pattern pattern;

    /**
     * Public constructor
     *
     * @param modifier    what should happen if mask doesn't match
     * @param textField   link to JTextField
     * @param regexDouble regex-mask
     */
    public UndoableEListenerImpl(BiConsumer<JTextField, UndoableEditEvent> modifier,
                                 JTextField textField, String regexDouble) {
        this.modifier = modifier;
        this.textField = textField;
        this.pattern = Pattern.compile(regexDouble);
    }

    /**
     * Public constructor
     *
     * @param modifier         what should happen if mask doesn't match
     * @param optionalModifier what should happen if mask matches
     * @param textField        link to JTextField
     * @param regexDouble      regex-mask
     */
    public UndoableEListenerImpl(BiConsumer<JTextField, UndoableEditEvent> modifier,
                                 BiConsumer<JTextField, UndoableEditEvent> optionalModifier,
                                 JTextField textField, String regexDouble) {
        this(modifier, textField, regexDouble);
        this.optionalModifier = optionalModifier;

    }

    /**
     * This method is handling the UndoableEditEvent using the mask and BiConsumers.
     * Result of matching can influence both on Document and JTextField
     *
     * @param e an {@code UndoableEditEvent} object
     * @throws IllegalStateException if there are problems with Document model
     */
    @Override
    public void undoableEditHappened(UndoableEditEvent e) throws IllegalStateException {
        String text;

        try {
            text = ((Document) e.getSource()).getText(0, ((Document) e.getSource()).getLength());
        } catch (BadLocationException ex) {
            throw new IllegalStateException("Internal error. Couldn't perform operation", ex);
        }

        Matcher matcher = pattern.matcher(text);
        if (!matcher.find()) {
            modifier.accept(textField, e);
        } else {
            if (optionalModifier != null) {
                optionalModifier.accept(textField, e);
            }
        }
    }
}
