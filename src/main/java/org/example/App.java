package org.example;

import org.example.calculator.arithmetic_processor_impl.DivideArithmeticProcessorImpl;
import org.example.calculator.arithmetic_processor_impl.MinusArithmeticProcessorImpl;
import org.example.calculator.arithmetic_processor_impl.MultipleArithmeticProcessorImpl;
import org.example.calculator.arithmetic_processor_impl.SumArithmeticProcessorImpl;
import org.example.calculator.models.ArithmeticProcessor;
import org.example.calculator.utils.ArithmeticOperation;
import org.example.gui_swing.MainWindow;
import org.example.calculator.service.CalculatorService;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

public class App {
    private static final String RESULT_FORMAT = "#0.000";

    public static void main(String[] args) {
        CalculatorService service = new CalculatorService(createArithmeticOperationMap(), RESULT_FORMAT);

        SwingUtilities.invokeLater(() -> new MainWindow(service));
    }

    private static HashMap<ArithmeticOperation, ArithmeticProcessor> createArithmeticOperationMap() {
        return new HashMap<>(Map.of(
                ArithmeticOperation.MINUS, new MinusArithmeticProcessorImpl(),
                ArithmeticOperation.SUM, new SumArithmeticProcessorImpl(),
                ArithmeticOperation.DIVIDE, new DivideArithmeticProcessorImpl(),
                ArithmeticOperation.MULTIPLE, new MultipleArithmeticProcessorImpl()
        ));
    }
}
