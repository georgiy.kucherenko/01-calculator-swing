package org.example.calculator.utils;

/**
 * This class holds all possible types of Arithmetic operations that can
 * be used in {@link org.example.calculator.service.CalculatorService}.
 * Each type of operation is represented by its String title also. This Title is used for GUI and logs.
 */
public enum ArithmeticOperation {
    /**
     * Operation of summarising
     */
    SUM(" + "),
    /**
     * Operation of deducting
     */
    MINUS(" - "),
    /**
     * Operation of multiplying
     */
    MULTIPLE(" * "),
    /**
     * Operation of dividing
     */
    DIVIDE(" / ");

    private final String title;

    ArithmeticOperation(String s) {
        title = s;
    }

    public String getTitle() {
        return title;
    }
}
