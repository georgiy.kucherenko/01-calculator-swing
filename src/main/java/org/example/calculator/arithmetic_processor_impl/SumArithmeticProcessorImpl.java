package org.example.calculator.arithmetic_processor_impl;

import org.example.calculator.models.ResultOfCalculation;
import org.example.calculator.models.ArithmeticProcessor;

/**
 * The class implements interface {@link ArithmeticProcessor}.
 * It provides the only one method of summarizing one double and another double.
 *
 * @author Georgiy Kucherenko
 */
public class SumArithmeticProcessorImpl implements ArithmeticProcessor {
    @Override
    public ResultOfCalculation calculate(Double a, Double b, String describingOfOperation) {
        return new ResultOfCalculation(a + b, a, b, describingOfOperation);
    }
}
