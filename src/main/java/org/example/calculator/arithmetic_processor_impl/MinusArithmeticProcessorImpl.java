package org.example.calculator.arithmetic_processor_impl;

import org.example.calculator.models.ArithmeticProcessor;
import org.example.calculator.models.ResultOfCalculation;

/**
 * The class implements interface {@link ArithmeticProcessor}.
 * It provides the only one method of subtraction one double from another double.
 *
 * @author Georgiy Kucherenko
 */
public class MinusArithmeticProcessorImpl implements ArithmeticProcessor {
    @Override
    public ResultOfCalculation calculate(Double a, Double b, String describingOfOperation) {
        return new ResultOfCalculation(a - b, a, b, describingOfOperation);
    }
}
