package org.example.calculator.models;

import java.text.DecimalFormat;

/**
 * Instances of this class encapsulate the results of arithmetic operation made by {@link ArithmeticProcessor}.
 * Results are represented by two fields: double value and string value
 * that describes both input data and the result of arithmetic operation.
 */
public class ResultOfCalculation {
    /**
     * the results of arithmetic operation made by {@link ArithmeticProcessor}
     */
    private final double result;
    /**
     * String representation of the whole operation. Example:
     * "5 + 2 = 7"
     */
    private final String stringForLog;
    /**
     * Holds the standard for rounding numerics with floating points.
     * Shows how many digits must be shown after the point.
     */
    private final static String RESULT_FORMAT = "#0.000";

    /**
     * Getter
     *
     * @return the results of arithmetic operation made by {@link ArithmeticProcessor}
     */
    public double getResult() {
        return result;
    }

    /**
     * Getter
     *
     * @return String representation of the whole operation. Example: "5 + 2 = 7"
     */
    public String getStringForLog() {
        return stringForLog;
    }

    /**
     * Public constructor
     *
     * @param result              the results of arithmetic operation made by {@link ArithmeticProcessor}
     * @param a                   the first numeric that was used in arithmetic operation
     * @param b                   the second numeric that was used in arithmetic operation
     * @param arithmeticOperation Sting representation of arithmetic operation that was made.
     *                            for example: " + ", or " - ", etc. These values are hold and taken
     *                            from {@link org.example.calculator.utils.ArithmeticOperation}
     */
    public ResultOfCalculation(double result, double a, double b, String arithmeticOperation) {
        this.result = result;
        this.stringForLog = String.format(
                "%s%s%s = %s",
                roundNumericAndConvertToString(a),
                arithmeticOperation,
                roundNumericAndConvertToString(b),
                roundNumericAndConvertToString(result));
    }

    private String roundNumericAndConvertToString(double doubleValue) {
        if (Math.abs(doubleValue % 2) != 1 && doubleValue % 2 != 0) {
            return new DecimalFormat(RESULT_FORMAT).format(doubleValue);
        } else {
            return String.valueOf((int) doubleValue);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResultOfCalculation)) return false;

        ResultOfCalculation that = (ResultOfCalculation) o;

        if (Double.compare(that.result, result) != 0) return false;
        return stringForLog.equals(that.stringForLog);
    }

    @Override
    public int hashCode() {
        int result1;
        long temp;
        temp = Double.doubleToLongBits(result);
        result1 = (int) (temp ^ (temp >>> 32));
        result1 = 31 * result1 + stringForLog.hashCode();
        return result1;
    }
}
