package org.example.calculator.service;

import org.example.calculator.models.ResultOfCalculation;
import org.example.calculator.utils.ArithmeticOperation;
import org.example.calculator.models.ArithmeticProcessor;

import java.text.DecimalFormat;
import java.util.Map;

/**
 * This is a service class for providing different arithmetic operations.
 * It is based on Strategy design pattern.
 * The only one field holds a Map with Arithmetic operation name as a key, and ArithmeticProcessor as a value.
 *
 * @author Georgiy Kucherenko
 */
public class CalculatorService {
    private String resultFormat;
    private final Map<ArithmeticOperation, ArithmeticProcessor> mapOfOperations;

    /**
     * The only one constructor.
     *
     * @param mapOfOperations holds a Map of pairs:
     *                        <p>{@link ArithmeticOperation} as a Key. Instance of Enum
     *                        <p>{@link ArithmeticProcessor} as a Mapped value. Instance that will be used for
     *                        making Arithmetic operation
     * @param resultFormat    represents a mask for rounding the result
     */
    public CalculatorService(Map<ArithmeticOperation, ArithmeticProcessor> mapOfOperations, String resultFormat) {
        this.mapOfOperations = mapOfOperations;
        this.resultFormat = resultFormat;
    }

    /**
     * @param a         is the first numeric to be applied in arithmetic operation
     * @param b         is the second numeric to be applied in arithmetic operation
     * @param operation instance of {@link ArithmeticOperation} enum. Indicates a type of Arithmetic operation.
     * @return {@link ResultOfCalculation} that holds numeric result and string representation of arithmetic operation.
     * @throws ArithmeticException in case of dividing by zero
     */
    public ResultOfCalculation getResultOfCalculation(Double a, Double b, ArithmeticOperation operation)
            throws ArithmeticException {
        return mapOfOperations.get(operation).calculate(a, b, operation.getTitle());
    }

    /**
     * Returns double value rounded according to configured format
     *
     * @param result value to be rounded
     * @return rounded value
     */
    public String getRoundedResult(double result) {
        if (Math.abs(result % 2) != 1 && result % 2 != 0) {
            return new DecimalFormat(resultFormat).format(result);
        } else {
            return String.valueOf((int) result);
        }
    }

    /**
     * Setter for format tuning
     *
     * @param resultFormat sets the format for rounding doubles
     */
    public void setResultFormat(String resultFormat) {
        this.resultFormat = resultFormat;
    }
}
