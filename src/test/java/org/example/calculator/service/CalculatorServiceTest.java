package org.example.calculator.service;

import org.example.calculator.arithmetic_processor_impl.*;
import org.example.calculator.models.ResultOfCalculation;
import org.example.calculator.utils.ArithmeticOperation;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorServiceTest {

    private static final String RESULT_FORMAT = "#0.000";
    private static final Double VALUE_A = 10.0;
    private static final Double VALUE_B = 5.0;
    private static final ArithmeticOperation MULTIPLE_OPERATION = ArithmeticOperation.MULTIPLE;
    private CalculatorService service;

    @BeforeEach
    void init() {
        service = new CalculatorService(new HashMap<>(Map.of(
                ArithmeticOperation.MINUS, new MinusArithmeticProcessorImpl(),
                ArithmeticOperation.SUM, new SumArithmeticProcessorImpl(),
                ArithmeticOperation.DIVIDE, new DivideArithmeticProcessorImpl(),
                ArithmeticOperation.MULTIPLE, new MultipleArithmeticProcessorImpl()
        )),
                RESULT_FORMAT);
    }

    @Test
    @DisplayName("When getResultOfCalculation() with all parameters receive result")
    void whenCallGetResultOfCalculationReceiveCorrectResult() {
        Assertions.assertEquals(
                service.getResultOfCalculation(VALUE_A, VALUE_B, MULTIPLE_OPERATION),
                new ResultOfCalculation(
                        VALUE_A * VALUE_B,
                        VALUE_A,
                        VALUE_B,
                        MULTIPLE_OPERATION.getTitle()
                )
        );
    }

    @ParameterizedTest
    @MethodSource("provideParamsForNullChecking")
    @DisplayName("When set Nulls to getResultOfCalculation() receive NPE")
    void whenSetNullToGetResultOfCalculationReceiveNPE(Double a, Double b, ArithmeticOperation operation) {
        assertThrows(
                NullPointerException.class,
                () -> service.getResultOfCalculation(a, b, operation)
        );
    }

    private static Stream<Arguments> provideParamsForNullChecking() {
        return Stream.of(
                Arguments.of(null, VALUE_B, MULTIPLE_OPERATION),
                Arguments.of(VALUE_A, null, MULTIPLE_OPERATION),
                Arguments.of(VALUE_A, VALUE_B, null)
        );
    }

    @Test
    @DisplayName("When call getRoundedResult() receive correct rounded data")
    void whenCallGetRoundedResultReceiveCorrectData() {
        service.setResultFormat("#0.00");
        assertEquals("3.33", service.getRoundedResult(3.3333333333));
        assertEquals("6.67", service.getRoundedResult(6.666666666666));
        assertEquals("7", service.getRoundedResult(7.0));
        assertEquals("-1", service.getRoundedResult(-1d));

        service.setResultFormat("#0.000");
        assertEquals("3.333", service.getRoundedResult(3.3333333333));
        assertEquals("6.667", service.getRoundedResult(6.666666666666));
        assertEquals("7", service.getRoundedResult(7.0));
        assertEquals("-1", service.getRoundedResult(-1d));

        service.setResultFormat(RESULT_FORMAT);
    }
}