package org.example.calculator.arithmetic_processor_impl;

import org.example.calculator.models.ResultOfCalculation;
import org.example.calculator.utils.ArithmeticOperation;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DivideArithmeticProcessorImplTest {
    private static final String DIVIDE = ArithmeticOperation.DIVIDE.getTitle();
    private static final String ARITHMETIC_EXCEPTION_FORMAT = "Cannot divide by zero.. you have entered %f / %f";
    private DivideArithmeticProcessorImpl divideProcessor;

    @BeforeEach
    void init() {
        divideProcessor = new DivideArithmeticProcessorImpl();
    }

    @ParameterizedTest
    @MethodSource("provideDataForDividingPositive")
    @DisplayName("When divide positive by positive")
    void whenDivideTwoPositivesReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                divideProcessor.calculate(a, b, DIVIDE)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForDividingNegative")
    @DisplayName("When divide negative by negative")
    void whenDivideTwoNegativesReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                divideProcessor.calculate(a, b, DIVIDE)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForDividingPositiveByNegative")
    @DisplayName("When divide positive by negative")
    void whenDividePositiveByNegativesReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                divideProcessor.calculate(a, b, DIVIDE)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForDividingNegativeByPositive")
    @DisplayName("When divide negative by positive")
    void whenDivideNegativeByPositiveReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                divideProcessor.calculate(a, b, DIVIDE)
        );
    }

    private static Stream<Arguments> provideDataByMap(Map<Double, Double> mapOfDoubles) {
        List<Arguments> arguments = new ArrayList<>();
        Double a, b;

        for (Map.Entry<Double, Double> entry : mapOfDoubles.entrySet()) {
            a = entry.getKey();
            b = entry.getValue();
            arguments.add(Arguments.of(
                    a, b, new ResultOfCalculation(a / b, a, b, DIVIDE)
            ));
        }
        return arguments.stream();
    }

    private static Stream<Arguments> provideDataForDividingPositiveByNegative() {
        return provideDataByMap(Map.of(
                10d, -2d,
                40d, -4d
        ));
    }

    private static Stream<Arguments> provideDataForDividingNegativeByPositive() {
        return provideDataByMap(Map.of(
                -10d, 2d,
                -40d, 4d
        ));
    }


    private static Stream<Arguments> provideDataForDividingPositive() {
        return provideDataByMap(Map.of(
                10d, 2d,
                40d, 4d
        ));
    }

    public static Stream<Arguments> provideDataForDividingNegative() {
        return provideDataByMap(Map.of(
                -10d, -2d,
                -40d, -4d
        ));
    }

    @Test
    @DisplayName("When trying to divide by Zero")
    void whenTryToDivideByZeroCatchArithmeticException() {
        Double a = 12.1;
        Double b = 0d;

        Exception exception = assertThrows(ArithmeticException.class, () -> divideProcessor.calculate(a, b, DIVIDE));

        String expectedMessage = String.format(ARITHMETIC_EXCEPTION_FORMAT, a, b);
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("When trying to put null ")
    void whenTryToPutNullCatchNullPointerException() {
        assertThrows(NullPointerException.class, () -> divideProcessor.calculate(null, 5.0, DIVIDE));
        assertThrows(NullPointerException.class, () -> divideProcessor.calculate(6.0, null, DIVIDE));
    }
}