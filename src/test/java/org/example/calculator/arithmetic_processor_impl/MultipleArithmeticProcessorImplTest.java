package org.example.calculator.arithmetic_processor_impl;

import org.example.calculator.models.ResultOfCalculation;
import org.example.calculator.utils.ArithmeticOperation;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class MultipleArithmeticProcessorImplTest {
    private static final String MULTIPLE = ArithmeticOperation.MULTIPLE.getTitle();
    private MultipleArithmeticProcessorImpl multipleProcessor;

    @BeforeEach
    void init() {
        multipleProcessor = new MultipleArithmeticProcessorImpl();
    }

    @ParameterizedTest
    @MethodSource("provideDataForMultiplyingPositive")
    @DisplayName("When multiple positive and positive")
    void whenMultipleTwoPositivesReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                multipleProcessor.calculate(a, b, MULTIPLE)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForMultiplyingNegative")
    @DisplayName("When multiple negative and negative")
    void whenMultiplyingTwoNegativesReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                multipleProcessor.calculate(a, b, MULTIPLE)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForMultiplyingPositiveByNegative")
    @DisplayName("When multiple positive and negative")
    void whenMultiplePositiveAndNegativesReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                multipleProcessor.calculate(a, b, MULTIPLE)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForMultiplyingNegativeByPositive")
    @DisplayName("When multiple negative and positive")
    void whenMultipleNegativeAndPositiveReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                multipleProcessor.calculate(a, b, MULTIPLE)
        );
    }

    private static Stream<Arguments> provideDataByMap(Map<Double, Double> mapOfDoubles) {
        List<Arguments> arguments = new ArrayList<>();
        Double a, b;

        for (Map.Entry<Double, Double> entry : mapOfDoubles.entrySet()) {
            a = entry.getKey();
            b = entry.getValue();
            arguments.add(Arguments.of(
                    a, b, new ResultOfCalculation(a * b, a, b, MULTIPLE)
            ));
        }
        return arguments.stream();
    }

    private static Stream<Arguments> provideDataForMultiplyingPositiveByNegative() {
        return provideDataByMap(Map.of(
                10d, -2d,
                40d, -4d
        ));
    }

    private static Stream<Arguments> provideDataForMultiplyingNegativeByPositive() {
        return provideDataByMap(Map.of(
                -10d, 2d,
                -40d, 4d
        ));
    }


    private static Stream<Arguments> provideDataForMultiplyingPositive() {
        return provideDataByMap(Map.of(
                10d, 2d,
                40d, 4d
        ));
    }

    public static Stream<Arguments> provideDataForMultiplyingNegative() {
        return provideDataByMap(Map.of(
                -10d, -2d,
                -40d, -4d
        ));
    }

    @Test
    @DisplayName("When trying to put null ")
    void whenTryToPutNullCatchNullPointerException() {
        assertThrows(NullPointerException.class, () -> multipleProcessor.calculate(null, 5.0, MULTIPLE));
        assertThrows(NullPointerException.class, () -> multipleProcessor.calculate(6.0, null, MULTIPLE));
    }
}