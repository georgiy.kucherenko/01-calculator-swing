package org.example.calculator.arithmetic_processor_impl;

import org.example.calculator.models.ResultOfCalculation;
import org.example.calculator.utils.ArithmeticOperation;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class SumArithmeticProcessorImplTest {
    private static final String SUM = ArithmeticOperation.SUM.getTitle();
    private SumArithmeticProcessorImpl sumProcessor;

    @BeforeEach
    void init() {
        sumProcessor = new SumArithmeticProcessorImpl();
    }

    @ParameterizedTest
    @MethodSource("provideDataForSummingPositive")
    @DisplayName("When sum positive and positive")
    void whenSumTwoPositivesReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                sumProcessor.calculate(a, b, SUM)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForSummingNegative")
    @DisplayName("When sum negative and negative")
    void whenSummingTwoNegativesReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                sumProcessor.calculate(a, b, SUM)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForSummingPositiveAndNegative")
    @DisplayName("When sum positive and negative")
    void whenSummingPositiveAndNegativeReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                sumProcessor.calculate(a, b, SUM)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForSummingNegativeAndPositive")
    @DisplayName("When sum negative and positive")
    void whenSummingNegativeAndPositiveReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                sumProcessor.calculate(a, b, SUM)
        );
    }

    private static Stream<Arguments> provideDataByMap(Map<Double, Double> mapOfDoubles) {
        List<Arguments> arguments = new ArrayList<>();
        Double a, b;

        for (Map.Entry<Double, Double> entry : mapOfDoubles.entrySet()) {
            a = entry.getKey();
            b = entry.getValue();
            arguments.add(Arguments.of(
                    a, b, new ResultOfCalculation(a + b, a, b, SUM)
            ));
        }
        return arguments.stream();
    }

    private static Stream<Arguments> provideDataForSummingPositiveAndNegative() {
        return provideDataByMap(Map.of(
                10d, -2d,
                40d, -4d
        ));
    }

    private static Stream<Arguments> provideDataForSummingNegativeAndPositive() {
        return provideDataByMap(Map.of(
                -10d, 2d,
                -40d, 4d
        ));
    }


    private static Stream<Arguments> provideDataForSummingPositive() {
        return provideDataByMap(Map.of(
                10d, 2d,
                40d, 4d
        ));
    }

    public static Stream<Arguments> provideDataForSummingNegative() {
        return provideDataByMap(Map.of(
                -10d, -2d,
                -40d, -4d
        ));
    }

    @Test
    @DisplayName("When trying to put null ")
    void whenTryToPutNullCatchNullPointerException() {
        assertThrows(NullPointerException.class, () -> sumProcessor.calculate(null, 5.0, SUM));
        assertThrows(NullPointerException.class, () -> sumProcessor.calculate(6.0, null, SUM));
    }
}