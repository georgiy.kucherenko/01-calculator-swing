package org.example.calculator.arithmetic_processor_impl;

import org.example.calculator.models.ResultOfCalculation;
import org.example.calculator.utils.ArithmeticOperation;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class MinusArithmeticProcessorImplTest {
    private static final String MINUS = ArithmeticOperation.MINUS.getTitle();
    private MinusArithmeticProcessorImpl minusProcessor;

    @BeforeEach
    void init() {
        minusProcessor = new MinusArithmeticProcessorImpl();
    }


    @ParameterizedTest
    @MethodSource("provideDataForDeductingTwoPositives")
    @DisplayName("When deduct positive from positive")
    void whenDeductPositiveFromPositiveReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                minusProcessor.calculate(a, b, MINUS)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForDeductingTwoNegatives")
    @DisplayName("When deduct negative from negative")
    void whenDeductNegativeFromNegativeReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                minusProcessor.calculate(a, b, MINUS)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForDeductingNegativeFromPositive")
    @DisplayName("When deduct negative from positive")
    void whenDeductNegativeFromPositiveReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                minusProcessor.calculate(a, b, MINUS)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDataForDeductingPositiveFromNegative")
    @DisplayName("When deduct positive from negative")
    void whenDeductPositiveFromNegativeReceiveCorrectResult(Double a, Double b, ResultOfCalculation expectedResult) {
        Assertions.assertEquals(
                expectedResult,
                minusProcessor.calculate(a, b, MINUS)
        );
    }

    private static Stream<Arguments> provideDataByMap(Map<Double, Double> mapOfDoubles) {
        List<Arguments> arguments = new ArrayList<>();
        Double a, b;

        for (Map.Entry<Double, Double> entry : mapOfDoubles.entrySet()) {
            a = entry.getKey();
            b = entry.getValue();
            arguments.add(Arguments.of(
                    a, b, new ResultOfCalculation(a - b, a, b, MINUS)
            ));
        }
        return arguments.stream();
    }

    private static Stream<Arguments> provideDataForDeductingNegativeFromPositive() {
        return provideDataByMap(Map.of(
                10d, -2d,
                40d, -4d
        ));
    }

    private static Stream<Arguments> provideDataForDeductingPositiveFromNegative() {
        return provideDataByMap(Map.of(
                -10d, 2d,
                -40d, 4d
        ));
    }

    private static Stream<Arguments> provideDataForDeductingTwoPositives() {
        return provideDataByMap(Map.of(
                10d, 2d,
                40d, 4d
        ));
    }

    public static Stream<Arguments> provideDataForDeductingTwoNegatives() {
        return provideDataByMap(Map.of(
                -10d, -2d,
                -40d, -4d
        ));
    }

    @Test
    @DisplayName("When trying to put null ")
    void whenTryToPutNullCatchNullPointerException() {
        assertThrows(NullPointerException.class, () -> minusProcessor.calculate(null, 5.0, MINUS));
        assertThrows(NullPointerException.class, () -> minusProcessor.calculate(6.0, null, MINUS));
    }
}